#!/bin/env python

def main():
    lc.start()
    lc.clean()
    lc.quit_script()

if __name__ == "__main__":
    if __package__ is None:
        # for calling as a script. e.g.
        #   $ ./main
        #   $ python logscleaning/main.py
        from os import path
        import sys
        sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
    # for calling as a package, too. e.g.
    #   $ python -m logscleaning.main
    from logscleaner import lc
    main()
