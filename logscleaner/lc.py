"""
Remove and clean files, according to a configuration set on conf.json file.
Writes to log file defined in LOG_FILE_NAME.
"""

import datetime
from os import path, stat, remove
import sys
import time

from pssapi.logger.logger import load_logger
from pssapi.utils import conf
from pssapi.cmdexe import cmd

logger = None
EXEC_DIR = path.dirname(path.realpath(sys.argv[0]))
LOG_FILE_NAME = path.join(EXEC_DIR, 'logs', 'logscleaner.log')
REM_EXE_FILE_NAME = path.join(EXEC_DIR, 'logscleaner', 'rmt_lc.py')
SERVER_REM_EXE_FILE_NAME = path.join(EXEC_DIR, 'logscleaner', '{0}-rmt_lc.py')
CONF_FILE_NAME = path.join(EXEC_DIR, 'conf', 'conf.json')

def start():
    """Load the logger object and print the initial time to log file."""
    global logger
    logger_fmtter = {
        'INFO': '%(message)s'
    }
    logger = load_logger(log_file_name=LOG_FILE_NAME, **logger_fmtter)
    logger.info('============================================================')
    logger.info(datetime.datetime.now())

def quit_script(error_message=None):
    """Print error_message and datetime into log file. Quit current script."""
    if error_message:
        logger.error(error_message)
    logger.info('Quiting now...')
    logger.info(datetime.datetime.now())
    logger.info('============================================================')
    exit()

def _add_cont2tmp_script(exe_file_name, tmp_exe_file_name, json_content):
    """Copy the source code to the script which will be executed remotely.
    Main content (source code) will be taken from existing Python script
    and the json configuration, which will be taken from the conf file, but 
    it will be passed as parameter.

    Keyword arguments:
    exe_file_name -- file containing the main source code.
    tmp_exe_file_name -- file to copy the source code to. It will become the 
        Python script to be executed remotely.
    json_content -- configuration in JSON format.

    Return True if operation of copying the whole content to the file was
    successful. Otherwise, return False.
    """
    json_content = "JSON_CONTENT = {0}".format(json_content)
    try:
        with open(exe_file_name) as f:
            with open(tmp_exe_file_name, 'w') as fw:
                fw.write(json_content)
                for line in f:
                    fw.write(line)
    except Exception as e:
        logger.error("Error copying content to temp script: {0}".format(str(e)))
        return False
    return True

def _create_tmp_script(tmp_exe_file_name, remove_after=5):
    """Check tmp_exe_file_name exists. If not, create it. If it already exists,
    remove it and create a new one if the existing one was modified 
    'remove_after' or more minutes ago (we are supposing the script might be 
    running if this file was modified less than 'remove_after' minutes ago).
    Return true if file was created. False otherwise.
    """
    _file = None
    try:
        if path.isfile(tmp_exe_file_name):
            # check when file was created
            _stat = stat(tmp_exe_file_name)
            time_in_seconds = time.time() - (remove_after * 60)
            if time_in_seconds <= _stat.st_mtime:
                # file was modified less than 'remove_after' minutes ago
                # file might be in use
                logger.error("The temp script already exists and it was " + 
                        "created {0} or less minutes.".format(remove_after))
                return False
            # remove file
            remove(tmp_exe_file_name)
        # create file
        _file = open(tmp_exe_file_name, 'w+')
    except Exception as e:
        logger.error("Error creating temp script: {0}".format(str(e)))
        return False
    finally:
        if _file:
            _file.close()
    return True

def clean():
    """Execute a temporary python script in the servers defined in the 
    conf file.
    Temporary python script will be created especifically for every server.
    """
    conf_json = conf.get_conf(CONF_FILE_NAME)
    logger.info("Cleaning servers...")
    for server in conf_json:
        logger.info("SERVER: {0}".format(server))
        tmp_exe_file_name = SERVER_REM_EXE_FILE_NAME.format(server)
        if not _create_tmp_script(tmp_exe_file_name) or \
           not _add_cont2tmp_script(REM_EXE_FILE_NAME, tmp_exe_file_name, 
                                    conf_json[server]):
            continue
        try:
            # tmp_exe_file_name was created and content was added
            # we can continue the execution for current server
            user = conf_json[server]['user']
            stdout, stderr = cmd.pythonexec(user, server, tmp_exe_file_name, 
                                            True)
            if stderr:
                logger.error(stderr)
            if stdout and stdout != '\n':
                logger.info(stdout)
            # remove script after been executed
            remove(tmp_exe_file_name)
        except Exception as e:
            logger.error(str(e))
