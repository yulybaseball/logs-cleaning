

"""
IMPORTANT!!

NEVER run this script directly!

This script is intended to be run remotely.
It will apply rules according to directories and/or files, according to what 
is defined in the dictionary passed as paratemer.
Script will print back a dictionary with the results of applying the rules.
"""

from datetime import datetime
import fnmatch
from os import path, stat, walk, remove, system
import time

RULE_CRITERIA = {'size': 'st_size', 'time': 'st_mtime', 
                 'name': 'fnmatch.fnmatchcase(_file, "{0}")'}
MEASUREMENTS = {'minutes': '*60', 'hours': '*60*60', 'days': '*60*60*24', 
                'years': '*60*60*24*365', 
                'kb': '*1024', 'mb': '*1024*1024', 'gb': '*1024*1024*1024'}

def _clear(_file):
    """Clear _file."""
    try:
        if path.exists(_file):
            system("> {0}".format(_file))
            return "Cleared: {0}".format(_file)
        else:
            return "File doesn't exist: {0}\n".format(_file)
    except Exception as e:
        return "Unable to clear file: {0}. Error: {1}\n".format(_file, str(e))

def _delete(_file):
    """Remove _file."""
    try:
        if path.exists(_file):
            remove(_file)
            return "Removed: {0}".format(_file)
        else:
            return "File doesn't exist: {0}\n".format(_file)
    except Exception as e:
        return "Unable to remove file: {0}. Error: {1}\n".format(_file, str(e))

def _zip(_file):
    """Zip _file."""
    try:
        if path.exists(_file):
            system("zip -q {0}.zip {1}".format(_file, _file))
            _delete(_file)
            return "Zipped: {0}".format(_file)
        else:
            return "File doesn't exist: {0}\n".format(_file)
    except Exception as e:
        return "Unable to zip file: {0}. Error: {1}\n".format(_file, str(e))

def _no_action(_file):
    return "There is no action defined for file {0}.\n".format(_file)

def _get_directory_files(directory, recursive=False):
    if recursive:
        items = walk(directory)
    else:
        items = [next(walk(directory))]
    for dirpath, _ ,filenames in items:
        for f in filenames:
            yield (f, path.abspath(path.join(dirpath, f)))

def _split_filter(f_filter):
    """Split filter into symbol,value pair."""
    f_no_spaces = "".join(f_filter.split())
    i = 0
    try:
        for symbol in f_no_spaces:
            if symbol.isdigit():
                return f_no_spaces[:i], int(f_no_spaces[i:])
            i += 1
    except Exception:
        pass
    return f_no_spaces, None

def _sizeof_fmt(num, suffix='B'):
    for unit in ['', 'Ki', 'Mi', 'Gi', 'Ti', 'Pi', 'Ei', 'Zi']:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)

def _get_filter_measurement_str(rule):
    measurement = MEASUREMENTS[rule['measurement']]
    filt = rule['filter']
    f_stat = " filestat.{0} ".format(RULE_CRITERIA[rule['criteria']])
    if rule['criteria'] != 'time':
        return "{0}{1}{2}".format(f_stat, filt, measurement)
    symbol, value = _split_filter(filt)
    return "(time.time() - ({0}{1})) {2} {3}".format(value, measurement, 
                                                     symbol, f_stat)

def _get_file_metadata(filestat):
    return (_sizeof_fmt(filestat.st_size), 
        datetime.fromtimestamp(filestat.st_mtime).strftime('%Y-%m-%d %H:%M'))

def _tabled_data(data, headers, sep='\uFFFA'):
    """Format and return a pretty printed table in plain text.

    Keyword arguments:
    data -- List containing other lists, which represent the rows on the 
    table. Please note the order of the values should match the same order of 
    the column name.
    headers -- List of the header names.
    sep -- Character to separate text inside the cell. Dummy character by 
    default.
    """
    table_str = ""
    sep = str(sep) if sep else '\n'
    all_data = []
    all_data.append(headers)
    for row in data:
        all_data.append(row)
    col_size = [
        max(map(len, (sep.join(col)).split(sep))) for col in zip(*all_data)
    ]
    frmt_str = ' | '.join(["{{{0}:<{1}}}".format(i, charlen) \
                            for i, charlen in enumerate(col_size)])
    line = frmt_str.replace(' | ', '-+-').format(*['-' * i for i in col_size])
    item = all_data.pop(0)
    line_done = False
    while all_data:
        if all(not i for i in item):
            item = all_data.pop(0)
            if line and (sep != '\uFFFA' or not line_done):
                table_str += (line + '\n')
                line_done = True
        row = [i.split(sep,1) for i in item]
        table_str += ((frmt_str.format(*[i[0] for i in row])) + '\n')
        item = [i[1] if len(i) > 1 else '' for i in row]
    return table_str

def _cleanup(directory, rules):
    action_result = []
    for _file_combo in _get_directory_files(directory):
        tmp_result = []
        _file = _file_combo[0]
        _file_complete_path = _file_combo[1]
        filestat = stat(_file_complete_path)
        for rule in rules:
            result = eval(rule['rule'])
            if result and path.exists(_file_complete_path):
                action = "_{0}(_file_complete_path)".format(rule.get('action', 
                                                            'no_action'))
                ar = _get_file_metadata(filestat)
                try:
                    tmp_result.append(eval(action))
                except NameError as e:
                    msg = ("NameError was thrown when trying to execute: " + 
                        "Action: {0}. File: {1}. Error: {2}").\
                        format(action, _file_complete_path, str(e))
                    tmp_result.append(msg)
                tmp_result.append(ar[1])
                tmp_result.append(ar[0])
                action_result.append(tmp_result)
    headers = ['Action: file', 'Edition date', 'Size']
    final_act_str = _tabled_data(action_result, headers)
    return final_act_str

def _build_rules(rules):
    rules_list = []
    for rules_group in rules:
        built_rules = ""
        action = ''
        for rule in rules_group:
            str_crt = RULE_CRITERIA[rule['criteria']]
            if built_rules:
                built_rules += ' and '
            if rule['criteria'] != 'name':
                built_rules += _get_filter_measurement_str(rule)
            else:
                # criteria is name
                r_filter = rule['filter'].replace('"', '\\\"').\
                                          replace('\'', '\\\'')
                name_filter = str_crt.format(r_filter)
                built_rules += "{0}".format(name_filter)
            if action and action != rule['action']:
                # actions are different, this is an error
                return []
            action = rule['action']
        rules_list.append({'rule': built_rules, 'action': action})
    return rules_list

def clean(conf_json_data):
    """"""
    result = "{0}\n".format(datetime.now())
    for directory in conf_json_data['paths']:
        result += ("DIRECTORY: {0}\n".format(directory))
        rules = _build_rules(conf_json_data['paths'][directory])
        result += ("RULES: {0}\n".format(rules))
        if rules:
            result += _cleanup(directory, rules)
        else:
            result += ("There was an error building the rules. " + 
                       "Please make sure every rule group (for the same " +
                       "path) has the same action.\n")
    print(result)

# call main clean method
# parameter JSON_CONTENT should be added through source code to the file 
# which is really running remotely
clean(JSON_CONTENT)
